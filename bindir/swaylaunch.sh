#!/bin/bash

source /etc/wlrenv

export XDG_SESSION_DESKTOP=sway
export XDG_CURRENT_DESKTOP=sway

systemd-cat --identifier=sway sway $@
