# Wayland Launch Scripts

User-set environment variables can cause issues when using multiple
desktops or swapping between X and wlroots window managers.

openSUSEway contains `sway-run.sh`, but I don't want the entirety of
their branding to have launch scripts like this. This aims to be an
alternative that aims to ship launchers such as that for every wayland
session in `openSUSE:Factory`.
