#!/bin/bash

source /etc/wlrenv

export XDG_SESSION_DESKTOP=Hyprland
export XDG_CURRENT_DESKTOP=Hyprland

systemd-cat --identifier=hypr Hyprland $@
